/*global define,require*/

define("webodf/editor/widgets/dopinusTags",[
    "dojo/ready",
    "dijit/MenuItem",
    "dijit/DropDownMenu",
    "dijit/form/Button",
    "dijit/form/DropDownButton",
    "dijit/Toolbar"],
    function (ready, MenuItem, DropDownMenu, Button, DropDownButton, Toolbar) {
    "use strict";
    return function DopinusTags(callback) {
        var that = this,
            dopinusTagsButon,
            paragraphFields,
            dopinusDropDownMenu,
            dopinusMenuItem={},
            dopinusDropDownMenu2,
            dopinusTagsButonLevel2,
            dopinusTagsButonLevel2Settings,
            dopinusTagsButonLevel3={},
            dopinusDropDownMenu3={},
            toolbar,
            tr,
            cursor,
            instanceCounter,
            instanceSet=1;
        
        
        
        
        $(document).bind("instanceSet", function(e,data) {
            instanceCounter=data.instanceCounterValue;
            if(instanceSet==1)
            {
                $("#webodfeditor-canvas"+instanceCounter).click(function(e){
                    cursor=e.toElement.childNodes;
                });
            }
            instanceSet++;
        });
        
        
        function DopinusTagButton(toolbar,tr)
        {
            that.dopinusTagsButon = new DropDownButton({
                dropDown: dopinusDropDownMenu,
                disabled: false,
                label: tr('Dopinus Tags'),
                iconClass: "dijitIconEditTask",
                style: {
                    float: 'left'
                }
            });
            that.dopinusTagsButon.placeAt(toolbar);
        
        }
        
        
        function insertDopinusTag(cursor, text){
        
           if(cursor!=undefined)
            {
                for(var i = 0; i<cursor.length;i++ )
                {
                    if(cursor[i].nodeName=="cursor")
                    {
                        if(typeof(cursor[i-1])!="undefined"&&typeof(cursor[i-1].nodeValue)!="undefined")
                        {
                            cursor[i-1].nodeValue = cursor[i-1].nodeValue+text;   
                        }
                        else
                        {
                            alert("V prazen dokument vstavljanje dopinus tagov ni dovoljeno");
                            
                            //console.log(cursor[i].parentElement.parentElement.innerHTML)//="&lt"+text.substr(1,text.length-2)+"&gt"+cursor[i].innerHTML
                        
                        }
                        $("#webodfeditor-canvas"+instanceCounter).select();
                        //console.log(cursor[i-1].parentElement.innerHTML);
  
                    }
                }
            }
            else{
                alert("Prosim izberite pozicijo vstavitve Dopinus taga");
            }
            
        
        }

        this.setDropDownDopinus = function(toolbar,tr,data) {
            
            dopinusDropDownMenu2 = new DropDownMenu({});
            

            dopinusDropDownMenu = new DropDownMenu({
                dropDown:dopinusDropDownMenu2
            });
            dopinusDropDownMenu2 = new DropDownMenu({
                dropDown:dopinusDropDownMenu3
            });

            

            //Level 2 - Components, settings, fields
            dopinusTagsButonLevel2 = new DropDownButton({
                dropDown: dopinusDropDownMenu2,
                disabled: false,
                label: tr('Fields'),
                iconClass: "dijitIconEditTask",
                style: {
                    float: 'left'
                }
            });
            dopinusTagsButonLevel2Settings = new DropDownButton({
                disabled: false,
                label: tr('Settings'),
                iconClass: "dijitIconEditTask",
                style: {
                    float: 'left'
                }
            });
            
            var tableInd = 0;
            for(var i = 0; i<data.data.length;)
            {
                //console.log(data.data[i]);
                var tmpTable = toArrayParser(data.data[i])[1];
              
                
                dopinusDropDownMenu3[tableInd] = new DropDownMenu({});
                if(typeof(data.data)!="undefined")
                {
                    while(i<data.data.length&&tmpTable==toArrayParser(data.data[i])[1])
                    {
                        dopinusMenuItem[i] = new MenuItem({
                            label: tr(toArrayParser(data.data[i])[2]),
                            value:""+data.data[i],
                            onClick:function(){
                            
                                if(typeof(cursor)=="undefined")
                                {
                                    //if click wasnt executed
                                    if(typeof($("#webodfeditor-canvas"+instanceCounter).find("body")[0].childNodes[0].childNodes[1].childNodes[1])!="undefined")
                                    {
                                        $("#webodfeditor-canvas"+instanceCounter).find("body")[0].childNodes[0].childNodes[1].childNodes[1].data=$("#webodfeditor-canvas"+instanceCounter).find("body")[0].childNodes[0].childNodes[1].childNodes[1].data+this.value;
                                        $("#webodfeditor-canvas"+instanceCounter).select();
                                    }
                                    else{
                                      
                                        alert("Vstavljanje dopinus tagov v prazen dokument ni mogoče");
                                    
                                    
                                    }
                 
                                }
                                else
                                {
                                    insertDopinusTag(cursor,this.value);
                                }

                                }
                        });
                        dopinusDropDownMenu3[tableInd].addChild(dopinusMenuItem[i]);


                        i++;
                    }
                }
                      //Level 3 - fields
                dopinusTagsButonLevel3[tableInd] = new DropDownButton({
                    dropDown: dopinusDropDownMenu3[tableInd],
                    disabled: false,
                    label: tr(tmpTable),
                    iconClass: "dijitIconEditTask",
                    style: {
                        float: 'left'
                    }
                });
                dopinusDropDownMenu2.addChild(dopinusTagsButonLevel3[tableInd]);
                tableInd++;
                
            }


            dopinusDropDownMenu.addChild(dopinusTagsButonLevel2);
            dopinusDropDownMenu.addChild(dopinusTagsButonLevel2Settings);
            
            DopinusTagButton(toolbar,tr);
            
                    //DOPINUS - END
        }
        
        function toArrayParser(tag){
            var str=tag.substring(tag.indexOf("_")+3,tag.lastIndexOf("-")-1);
            return str.split("/");

        
        }
        
        this.setEditorSession = function (session) {            
            //console.log("Hello I'm Dopinus Tag file");
            
                
        };

        this.onToolDone = function () {};
        
        

    };
});
