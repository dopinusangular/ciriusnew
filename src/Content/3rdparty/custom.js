/**
<ul class="nav navbar-nav navbar-right">
    <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="#about"><i class="fa fa-shield"></i> About</a></li>
    <li><a href="#contact"><i class="fa fa-comment"></i> Contact</a></li>
</ul>
*/
var cbQueue = [];
var isUilLoaded = false;
var mainComponentInfo = null;




function getAge(dob) {
  var now = new Date();
  var today = new Date(now.getYear(),now.getMonth(),now.getDate());

  var yearNow = now.getYear();
  var monthNow = now.getMonth();
  var dateNow = now.getDate();

  var yearDob = dob.getYear();
  var monthDob = dob.getMonth();
  var dateDob = dob.getDate();
  var age = {};


  yearAge = yearNow - yearDob;

  if (monthNow >= monthDob)
    var monthAge = monthNow - monthDob;
  else {
    yearAge--;
    var monthAge = 12 + monthNow -monthDob;
  }

  if (dateNow >= dateDob)
    var dateAge = dateNow - dateDob;
  else {
    monthAge--;
    var dateAge = 31 + dateNow - dateDob;

    if (monthAge < 0) {
      monthAge = 11;
      yearAge--;
    }
  }
  if(yearAge < 0)
      yearAge=0;
  if(monthAge < 0)
      monthAge = 0;
  if(dateAge < 0)
      dateAge = 0;

  age = {years: yearAge,months: monthAge,days: dateAge};

  return age;
}

/**
*
*/
function getDateAsStringDots(date){
    if(!date)
        return null;

    var month = '' + (date.getMonth() + 1);
    var day = '' + date.getDate();
    var year = date.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day,month, year].join('.');
}

/**
* Expects yyyy-dd-mm
*/
function getStringAsDateLines(str){
    if(!str || str.length == 0)
        return null;

    var parts = str.split("-");
    if(parts.length != 3)
        return null;

    return new Date(parts[0], parts[1]-1, parts[2]);
}

function e2ShowWarning(title, message, type){
    var element = $('#modalPopup');

    element.find(".modal-title").html(title);
    element.find(".modal-body").html(message);
    element.modal('show');
}
/**
* Expects dd.mm.yyyy format
*/
function getStringAsDate(str){
    if(!str || str.length == 0)
        return null;

    var parts = str.split(".");

    if(parts.length != 3)
        return null;

    return new Date(parts[2], parts[1]-1, parts[0]);
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function sortArrayOfObjByKey(arr){
    return arr.sort(function(a,b){
        if(a["Index"] < b["Index"])
            return -1;
        if(a["Index"] > b["Index"])
            return 2;
        return 0;
    });
}

/**********************************************************/
function getMainTableInfo(info){
    if(!info || !info.data || info.data.length == 0)
        return null;

    for(var i=0;i<info.data.length;i++){
        if(info.data[i].entity && info.data[i].entity.indexOf(CiriusConfig.tables.mainTable) > -1)
            return info.data[i];
    }

    return null;

}

function getMainTableMetaData(info){
    if(!info || !info.subComponents)
        return null;

    if(info.subComponents[CiriusConfig.tables.mainTableMetaData])
        return info.subComponents[CiriusConfig.tables.mainTableMetaData];

    return null;
}


function getMainComponentInfo(cb, ctx){
    if(mainComponentInfo)
        return cb.call(ctx||this, mainComponentInfo);

    var obj = {
        "method" : "Component.GetComponentInfo",
        "params" : {"component":"test/Ciri"},
        "id" : uil.core.config.GetRandom()
    };

    var that = this;
    uil.misc.Generic.sendRequest(obj, function (data) {
        mainComponentInfo = data.result;
        return cb.call(ctx||this, mainComponentInfo);
    },this);
}
/**********************************************************/

function clearCbQueue(){
    if(cbQueue.length == 0)
        return;

    for(var i=0;i<cbQueue.length;i++){
        if(!cbQueue[i].cb)
            continue;

        cbQueue[i].cb.call(cbQueue[i].ctx||this);
    }

    cbQueue = [];
}
/**
*
*/
function initUil(){
    qx.theme.manager.Meta.getInstance().setTheme(qx.Theme.getByName("uil.theme.IndigoTheme"));
    uil.core.logger.Init.getInstance().Create();
    uil.qx.patches.Patches.getInstance().initPatches();
    var mainScreen = new uil.components.execute.memoryICLinker.Component();
    mainScreen.setId("ciri");
    mainScreen.expose();

    loadSessonData(function(){
        isUilLoaded = true;
        clearCbQueue();
    },this);
}
/**
*
*
*/
function loadSessonData(cb, ctx){
    var req = {
        "method": "Auth.GetSessionInfo",
        "params": true,
        "id": uil.core.config.GetRandom()
    };
    uil.misc.Generic.sendRequest(req, function (data, xhr) {
        var _sessionStorage = uil.core.SessionStorage.getInstance();
        _sessionStorage._storeSessionData(data.result);
        cb.call(ctx||this);
    },this);
}

/**
* Upload a file to the server /File path
*
*@param e {object} click event on uploader
*@param cb {function(fileId, errorCode, errorMsg)}
*/

function uploadFileSubmit(e, cb, ctx){
    if(!cb)
        return console.error("No callback event given for file upload");

    if(!e)
        return cb.call(ctx||this, null, 1, "No file submit event found");



    var file = e.target.files[0];
    e.preventDefault();
    e.stopPropagation();

    if (!file)
        return cb.call(ctx||this, null, 2, "Please select a file to upload!");

    var data = null;
    if (window.FormData) {
        data = new FormData();
        data.append("file", file);
    }

    $.ajax({
        url: "File",
        type: "POST",
        async: true,
        cache: false,
        data: data,
        contentType: false, // We're sending the default content, which for jQuery is 'application/x-www-form-urlencoded; charset=UTF-8'...
        dataType: 'json', // ...and this is what we're expecting to get back.
        processData: false, // No processing, please. We're uploading a file as is.
        success: function (result) {
            if (result && result.success && result.id) {
                console.log("File successfully uploaded. ID: " + result.id);
                return cb.call(ctx||this, result.id);
            } else {
                return cb.call(ctx||this, null, 3, "Unknon error occured");
            }
        },
        error: function (xhr, status, event) {
            return cb.call(ctx||this, null, 4, "Upload failed with status " + status + ". Details: " + xhr.status + " - " + xhr.statusText);
            console.log("Upload failed with status " + status + ". Details: " + xhr.status + " - " + xhr.statusText);
        }
    });
}
/**
* Angular routing crap (todo: learn it)
*
*/

/**
* Reads the settings from the config file. And sets the css global variables
*
*/

/**
* Check if we are running from disk
*
*/
function isLocalRunning(){
    return location.hostname==="";
}
/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
/*$(function() {
    $("#side-menu").metisMenu({
        preventDefault: false, // mogoč bo delal na hover
        parentTrigger: '.nav-item',
        triggerElement: '.nav-link'
    });
});*/




//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent();

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
});

//multiselect enable
//
/*$(document).ready(function() {
        $(".ms").multiselect({
            enableFiltering: true,
            maxHeight: 200
        });
    });*/




