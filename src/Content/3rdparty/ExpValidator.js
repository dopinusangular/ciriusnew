﻿function getValues(obj, key) 
{ // returns value from json object
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (ExpIsObject(obj[i])) 
        {
            objects = objects.concat(getValues(obj[i], key));
        }
        else if (i == key) 
        {
            objects.push(obj[i]);
        }
    }
    //ALES FIX for Unknown fields names. Bad fix
    if(objects.length == 0)
        return [null];
    
    return objects;
}

function ExpIsObject (value) {
    return value && typeof value === 'object' && value.constructor === Object;
};

function IsDateValid(input) 
{ // checks if the date is valid
    var bits = input.split('-');
    var d = new Date(bits[0], bits[1] - 1, bits[2]);
    return d.getFullYear() == bits[0] && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[2]);
}
/**
* Checks to see if the expression is a function. It functions the same as "IsFunction(string)" function
* but not using regular expressions because of preformance issues
*/
function IsFunction2(string){
    if(string.indexOf("(") < 0 || string.indexOf(")"))
        return false;
    if (string.search('[A-Za-z]+\\(.+[A-Za-z]+.+\\,.+[0-9]+.+\\,.+[0-9]+.+\\)') > -1 || string.search('[A-Za-z]+\\(.+[A-Za-z]+.+\\,.+[0-9]+.+\\)') > -1 || string.search('[A-Za-z]+\\(.+[A-Za-z]+.+\\)') > -1) 
    {
        return true;
    }
    return false;
}

function IsFunction(string) // checks if string is function (for instance LEFT(name,3)...)
{
    if (string.search('[A-Za-z]+\\(.+[A-Za-z]+.+\\,.+[0-9]+.+\\,.+[0-9]+.+\\)') > -1 || string.search('[A-Za-z]+\\(.+[A-Za-z]+.+\\,.+[0-9]+.+\\)') > -1 || string.search('[A-Za-z]+\\(.+[A-Za-z]+.+\\)') > -1) 
    {
        return true;
    }
    return false;
}
var value = "";
var myValue = "";

/**
* Replaces all the instances of field keys in the expression with the actuals values
*
*@param expression {string}
*@param fields {object}
*
*@return {string}
*/
function replaceFields(expression, fields){
    fields = JSON.parse(fields);
    for(var key in fields){
        if(expression.indexOf(key) > -1)    //For speed purpose, skip most of the keys and save time on rexexp and replace
            expression = expression.replace(new RegExp(key,'g'), "\""+fields[key]+"\"");
    }
    return expression;
}
//When validating, we first check for functions in expression. If there are any, we evaluate them first and replace them in expression with their value.
//Next, we find expression within the innermost parentheses. We do that until all parentheses are removed - and all expressions evaluated.
//In the end we evaluate full expression.
function validateExp(expression, fields, number) {  // validates the expression
    var isLiteral = false;
    var withoutSpaces = "";
    //console.time("top");
    for (var i = 0; i < expression.length; i++) 
    { 
        var character = expression[i];
        if(character==" "){
            if(isLiteral)
                withoutSpaces+= character;
        }else{
            if(isLiteral && character=="\"")
                isLiteral = false
            else if(character=="\"")
                isLiteral = true;

            withoutSpaces+=character;
        }
            
        // remove whitespaces from expression for easier parsing
        /*if (expression.search(" ") > -1) 
        {
            expression = expression.replace(" ", "");
        }*/
    }
    expression = withoutSpaces;
    
    if (expression.split("(").length != expression.split(")").length && number == null) 
    {
        alert("Invalid expression! You may have some extra parentheses!");
        return;
    }
    //console.timeEnd("top");
    //console.time("mid");
    var string = expression;
    var json = JSON.parse(fields); // parse json string
    if (number == null || number == 2) 
    {
        /*if (expression.search("^AND") > -1 || expression.search("$AND") > -1 || expression.search("^OR") > -1 || expression.search("$OR") > -1) 
        { // if expression starts or ends with AND or OR it is not valid
            alert("Invalid expression");
            return;
        }*///ALES
        if(expression.search(/^AND|$AND|^OR|$OR/) > -1){
            console.error("Invalid expression");
            return;
        }
        
        /*for (var j = 0; j < expression.length; j++) 
        { // find ANDs and ORs and replace them with && and ||
            if (expression.search("AND") > -1) 
            {
                string = string.replace("AND", "&&");
            }
            if (expression.search("OR") > -1) 
            {
                string = string.replace("OR", "||");
            }
        }*///ALES
        string = string.replace(/AND/g, '&&');
        string = string.replace(/OR/g, '||');
    }
    var stringArray = new Array();
    stringArray = string.split(/[&|]{2}/); // split the expression

    var operators = new Array();
    operators.push("<", ">", "=", "==", "<=", ">=", "<>", "ends", "contains", "starts"); //define all operators
    if (number == null) 
    { // if number is not null, we are evaluating a function
        for (var i = 0; i < stringArray.length; i++) 
        {
            if (IsFunction2(stringArray[i])) 
            {
                if (stringArray[i].search("BETWEEN") == -1)
                 { // between has an end parenthesis so we don't want to remove it
                     while (stringArray[i].charAt(0) == "(") 
                    { // remove any extra parentheses
                        stringArray[i] = stringArray[i].slice(1, stringArray[i].length);
                    }
                    while (stringArray[i].charAt(stringArray[i].length - 1) == ")") {
                        stringArray[i] = stringArray[i].slice(0, stringArray[i].length - 1);
                    }
                }
                var temp = validateExp(stringArray[i], fields, 1); //evaluate function
                string = string.replace(stringArray[i], temp);
                stringArray.splice(i, 1); //remove function from array
                i--;
            }
        }
        while (string.indexOf("(") > -1 && string.indexOf(")") > -1) 
        {
            var tempExpression = string.slice(string.lastIndexOf("(") + 1, string.indexOf(")"));
            var temp = validateExp(tempExpression, fields);
            string = string.replace(string.charAt(string.lastIndexOf("(")), "");
            string = string.replace(string.charAt(string.indexOf(")")), "");
            string = string.replace(tempExpression, temp);
        }
    }
    //console.timeEnd("mid");
    //console.time("bigFor");
    for (var k = 0; k < stringArray.length; k++) 
    {
        stringArray[k] = stringArray[k].trim();
        var isBetween = false;
        if (stringArray[k].indexOf("BETWEEN(") > -1) //between is special because it doesn't have any operators ...
        {
            isBetween = true;
        }
        for (var l = 0; l < operators.length; l++) 
        {
            
            if (stringArray[k].search(operators[l]) > -1 || isBetween == true) 
            {
                if (stringArray[k].split(operators[l]).length != 2 && isBetween == false) 
                {
                    alert("Invalid operator!");
                    return;
                }
				//bug for operators <>,<=,>=
				if((operators[l]=="<"||operators[l]==">")&&stringArray[k].length>(stringArray[k].search(operators[l])+1))
				{
					if(stringArray[k][stringArray[k].search(operators[l])+1]==">"||stringArray[k][stringArray[k].search(operators[l])+1]=="=")
					{
						var ss=operators[l]+stringArray[k][stringArray[k].search(operators[l])+1];
						l=operators.indexOf(operators[l]+stringArray[k][stringArray[k].search(operators[l])+1]);
					}
				}
                // if string contains valid operator or string is between function continue, otherwise string is not valid expression                                                                    
                var isValidFunction = IsFunction(stringArray[k]);
                if (isValidFunction) 
                { // if string is function we parse key differently
                    if (stringArray[k].search(",") > -1) 
                    {
                        key = stringArray[k].slice(stringArray[k].search("\\([A-Za-z]") + 1, stringArray[k].search(","));
                        if (key.indexOf("(") > -1 || key.indexOf(")") > -1) // if key has some extra parentheses we remove them
                        {
                            key = key.replace("(", "");
                            key = key.replace(")", "");
                        }
                    }
                    else 
                    {
                        key = stringArray[k].slice(stringArray[k].indexOf("(") + 1, stringArray[k].indexOf(")")); //some functions only have one argument
                    }
                }
                else 
                {
                    //if not function we simply parse the word before operator
                    var key = stringArray[k].slice(stringArray[k].search("[\"]*[A-Za-z0-9]+"), stringArray[k].search(operators[l]));
                }
                
				var literalValue = false;
				
				if((key.charAt(0) == "\"" ||key.charAt(0) == "'") &&(key.charAt(key.length-1) == "\"" ||key.charAt(key.length-1) == "'"))
				{ 
				literalValue=true;
				}
                else 
				{
				var srch = fields.search(key);
				}
				//NOTE : Quick fix for fields that are not given in data input. We assume they are null - bad fix (Ales to blame)
                /*if(srch<0 && string.indexOf("=null") > 0)
                    return true;*/
                
				// if key does not exist in json object, we can't continue, because we don't have any value  for comparison 
				/*if(srch <0)
					return;
                else */if ((/*srch > -1 &&*/ (key.charAt(0) != "\"" ||key.charAt(0) != "'") &&(key.charAt(key.length-1) != "\"" ||key.charAt(key.length-1) != "'")) || literalValue==true ) 
                {
                    if(!literalValue)
					{
                    value = getValues(json, key);
                    value = value.toString();
					value = getValues(json, key).toString();
					
					} // extracts value from json object
					else
					{
					value=key.toString();
					
					}
                    
                   var operator = operators[l];
                    //if there is Fn. keyword this means we have to take value  from this field name
                    if (stringArray[k].indexOf("Fn.") > -1) 
                    {
                        myValue = getValues(json, stringArray[k].slice(stringArray[k].indexOf("Fn.") + 3, stringArray[k].length)).toString();
                    }
                    else 
                    {
                        if (stringArray[k].indexOf(")") > -1) //if user inputs too much parentheses, we fix this for them (for instance LEFT((name),10))...
                        {
                            var num = stringArray[k].indexOf(")");
                        }
                        else 
                        {
                            num = stringArray[k].length;
                        }
                        myValue = stringArray[k].slice(stringArray[k].search(operators[l]) + operator.length, stringArray[k].search(operators[l]) + num);
                    }
                    if (isValidFunction) 
                    {
                        var func = stringArray[k].slice(0, stringArray[k].indexOf("("));
                        //this switch statement checks if function exists
                        //if it does it checks if user passed right arguments, otherwise, we cannot use function
                        //then we convert function to native javascript method
                        switch (func) 
                        {
                            case "LEFT":
                                {
                                    if (stringArray[k].search('[A-Za-z]+\\(.*[A-Za-z]+[^,]*,[^,]*[0-9]+[^,]*\\)') > -1) 
                                    {
                                        var param = stringArray[k].slice(stringArray[k].search(",") + 1, stringArray[k].lastIndexOf(")"));
                                        if (param.indexOf("(") > -1 && param.indexOf(")") > -1) 
                                        {
                                            param = param.replace("(", "");
                                            param = param.replace(")", "");
                                        }
                                        value = value.slice(0, param);
                                    }
                                    else 
                                    {
                                        alert("Invalid function syntax!");
                                        return;
                                    }
                                    break;
                                }
                            case "RIGHT":
                                {
                                    if (stringArray[k].search('[A-Za-z]+\\(.*[A-Za-z]+[^,]*\\,[^,]*[0-9]+[^,]*\\)') > -1) 
                                    {
                                        var param = parseInt(stringArray[k].slice(stringArray[k].search(",") + 1, stringArray[k].indexOf(")")),10);
                                        value = value.slice(value.length - param, value.length);
                                    }
                                    else 
                                    {
                                        alert("Invalid function syntax!");
                                        return;
                                    }
                                    break;
                                }
                            case "SUBSTR":
                                {
                                    if (stringArray[k].search('[A-Za-z]+\\(.*[A-Za-z]+[^,]*\\,[^,]*[0-9]+.*\\,[^,]*[0-9]+[^,]*\\)') > -1) 
                                    {
                                        var param = parseInt(stringArray[k].slice(stringArray[k].indexOf(",") + 1, stringArray[k].lastIndexOf(",")),10);
                                        var param1 = parseInt(stringArray[k].slice(stringArray[k].lastIndexOf(",") + 1, stringArray[k].indexOf(")")),10);
                                        value = value.substring(param, param1);
                                    }
                                    else 
                                    {
                                        alert("Invalid function syntax!");
                                        return;
                                    }
                                    break;
                                }
                            case "TRIM":
                                {
                                    if (stringArray[k].search('[A-Za-z]+\\([^,]*[A-Za-z]+[^,]*\\)') > -1) 
                                    {
                                        value = value.trim();
                                    }
                                    else 
                                    {
                                        alert("Invalid function syntax!");
                                        return;
                                    }
                                    break;
                                }
                            case "DAY":
                                {
                                    if (stringArray[k].search('[A-Za-z]+\\([^,]*[A-Za-z]+[^,]*\\)') > -1) 
                                    {
                                        if (IsDateValid(value)) 
                                        {
                                            var date = new Date(value);
                                            value = date.getDate().toString();
                                        }
                                        else 
                                        {
                                            alert("Property is not date!");
                                            return;
                                        }
                                    }
                                    else 
                                    {
                                        alert("Invalid function syntax!");
                                        return;
                                    }
                                    break;
                                }
                            case "MONTH":
                                {
                                    if (stringArray[k].search('[A-Za-z]+\\([^,]*[A-Za-z]+[^,]*\\)') > -1) 
                                    {
                                        if (IsDateValid(value)) 
                                        {
                                            var date = new Date(value);
                                            value = (date.getMonth() + 1).toString();
                                        }
                                        else 
                                        {
                                            alert("Property is not date!");
                                            return;
                                        }
                                    }
                                    else 
                                    {
                                        alert("Invalid function syntax!");
                                        return;
                                    }
                                    break;
                                }
                            case "YEAR":
                                {
                                    if (stringArray[k].search('[A-Za-z]+\\([^,]*[A-Za-z]+[^,]*\\)') > -1) 
                                    {
                                        if (IsDateValid(value)) 
                                        {
                                            var date = new Date(value);
                                            value = date.getFullYear().toString();
                                        }
                                        else 
                                        {
                                            alert("Property is not date!");
                                            return;
                                        }
                                    }
                                    else 
                                    {
                                        alert("Invalid function syntax!");
                                        return;
                                    }
                                    break;
                                }
                            case "DOW":
                                {
                                    if (stringArray[k].search('[A-Za-z]+\\([^,]*[A-Za-z]+[^,]*\\)') > -1) {
                                        if (IsDateValid(value)) {
                                            var date = new Date(value);
                                            switch (date.getDay()) {
                                                case 0:
                                                    value = "sunday";
                                                    break;
                                                case 1:
                                                    value = "monday";
                                                    break;
                                                case 2:
                                                    value = "tuesday";
                                                    break;
                                                case 3:
                                                    value = "wednesday";
                                                    break;
                                                case 4:
                                                    value = "thursday";
                                                    break;
                                                case 5:
                                                    value = "friday";
                                                    break;
                                                case 6:
                                                    value = "saturday";
                                                    break;
                                                default:
                                                    alert("Invalid day");
                                                    return;
                                                    
                                            }
                                        }
                                        else 
                                        {
                                            alert("Property is not date!");
                                            return;
                                        }
                                    }
                                    else 
                                    {
                                        alert("Invalid function syntax!");
                                        return;
                                    }
                                    break;
                                }
                            case "BETWEEN":
                                {
                                    if (stringArray[k].search('[A-Za-z]+\\(.*[A-Za-z]+[^,]*\\,[^,]*[0-9]+.*\\,[^,]*[0-9]+[^,]*\\)') > -1) 
                                    {
                                        myValue = getValues(json, key);
                                        var param = parseInt(stringArray[k].slice(stringArray[k].indexOf(",") + 1, stringArray[k].lastIndexOf(",")),10);
                                        var param1 = parseInt(stringArray[k].slice(stringArray[k].lastIndexOf(",") + 1, stringArray[k].indexOf(")")),10);
                                        string = string.replace(stringArray[k], myValue + "<\"" + param1 + "\"&&" + myValue + ">\"" + param + "\"");
                                        isBetween = false;
                                        break;
                                    }
                                    else 
                                    {
                                        alert("Invalid function syntax!");
                                        return;
                                    }
                                }
                            default:
                                {
                                    alert("Invalid function");
                                    return;
                                }
                        }
                    }
                    //some filter operators are not valid javascript operators for logical expressions, so we convert them to native javascript
					if(!literalValue) {
                        value = "\""+value+"\""; 
                        literalValue=false;
					}
                    
                    switch (operator) 
                    {
                        case "=":
                            {
                                value = value.toLowerCase();
                                myValue = myValue.toLowerCase();
                                operator = "==";
                                string = string.replace(stringArray[k],value +  operator + myValue);
                                break;
                            }
                        case "<>":
                            {
                                operator = "!=";
                                string = string.replace(stringArray[k], value +  operator + myValue);
                                break;
                            }
                        case "contains":
                            {
                                if (value.search(myValue) > -1) 
                                {
                                    string = string.replace(stringArray[k], "true");
                                }
                                else 
                                {
                                    string = string.replace(stringArray[k], "false");
                                }
                                break;
                            }
                        case "starts":
                            {
                                //removed (") from both values before comparisson.
                                //reason: if we tryed to check if "abc" starts with "a", it always returned with false, because the (") signs were also compared, not only (a).     
                                myValue = myValue.replace(/"/g, "");
                                value = value.replace(/"/g, "");
                                
                                if (value.search(myValue) == 0) 
                                {
                                    string = string.replace(stringArray[k], "true");
                                }
                                else 
                                {
                                    string = string.replace(stringArray[k], "false");
                                }
                                break;
                            }
                        case "ends":
                            {
                                if (value.search(myValue) == value.length - myValue.length) 
                                {
                                    string = string.replace(stringArray[k], "true");
                                }
                                else 
                                {
                                    string = string.replace(stringArray[k], "false");
                                }
                                break;
                            }
                        case "==":
                        case ">":
                        case "<":
                        case "<=":
                        case ">=":
                            string = string.replace(stringArray[k], value +  operator + myValue);
                            break;
                        default:
                            {
                                alert("Invalid operator!");
                                return;
                            }
                    }
					literalValue=false;
                }
				else 
				{
				alert("Unknown error!");
				}
            }
        }
    }
    //console.timeEnd("bigFor");
    //console.time("bot");
    string = string.replace("(", "");
    string = string.replace(")", "");
	var bool = false;
    try
	{
		//console.log(string + "->" + eval(string)); 
		bool = eval(string);
		
	}
	catch(err)
	{
		bool=false;
	}
    //console.timeEnd("bot");
    return bool;
    //console.log(string );
   
}