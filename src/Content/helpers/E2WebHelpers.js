/**
* This class requires E2Helpers class to operate (E2Helpers.js)
*
*
*/
/**************************************************************************/
/*******************************Basic JS helpers***************************/
/**************************************************************************/

var E2JSHelpers = new function(){
    
}

E2JSHelpers.getUrlParameter = function(sParam){
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            if(sParameterName.length > 2){
                var ret = "";
                for(var y = 1;y<sParameterName.length;y++)
                    ret += sParameterName[y]+"=";
                return E2JSHelpers.rtrim(ret, "=");
            }
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

E2JSHelpers.hasAttribute = function($el, attribute){
    if(!$el || !attribute)
        return false;
    
    var attr = $el.attr(attribute);
    
    if(typeof attr !== typeof undefined && attr !== false)
        return true;
    
    return false;
}

E2JSHelpers.trim = function(str, chr) {
  var rgxtrim = (!chr) ? new RegExp('^\\s+|\\s+$', 'g') : new RegExp('^'+chr+'+|'+chr+'+$', 'g');
  return str.replace(rgxtrim, '');
}

E2JSHelpers.rtrim = function(str, chr) {
  var rgxtrim = (!chr) ? new RegExp('\\s+$') : new RegExp(chr+'+$');
  return str.replace(rgxtrim, '');
}

E2JSHelpers.ltrim = function(str, chr) {
  var rgxtrim = (!chr) ? new RegExp('^\\s+') : new RegExp('^'+chr+'+');
  return str.replace(rgxtrim, '');
}
/**************************************************************************/
/*******************************E2 CRUD************************************/
/**************************************************************************/

var E2CRUDMethods = new function(){
}

/**
* Submit trigger for the Pinegrow generated button
*
* Attributes of the HTML element hold the info for storing. Most are exclusive
* e2Entity (required) : entity name

* id-hidden-at : selector for "hidden" id-hidden-at
* id-filter-from-url : boolean, read "filter" parameter from url
* id-fixed : fixed value for modify 

* Event actions:
* before-save-selector :
* before-save-css :

* after-save-selector : 
* after-save-css :
 
* before-save-js :

* after-save-js :
*
*error-save-selector
*error-save-css
*error-save-js
*/
E2CRUDMethods.SubmitPG = function(element){
    if(!element || !($(element))){
        console.error("DOM element input param missing");
        return;
    }
    var $el = $(element);
    
    var entity = $el.attr("e2entity");
    
    if(!entity){
        console.error("No e2entity attribute found");
        return;
    }
    
    if($el.hasClass("disabled")){
        console.log("Button disabled. Waiting for response?");
        return;
    }
    
    var filter = E2CRUDMethods.__resolvePGButtonFilter($el);
    var values = E2CRUDMethods.__getInputValues(entity);
    
    if(values.length == 0){
        console.error("No values to insert/modify");
        return;
    }
    
    E2CRUDMethods.BeforeSubmit($el);
    $el.addClass("disabled");
    if(filter){
        E2CRUDMethods.UpdateExistingRecord(entity, values, filter, function(e){
            E2CRUDMethods.AfterSubmit($el,e);
            $el.removeClass("disabled");
        });
    }else{
        E2CRUDMethods.CreateNewRecord(entity, values, function(e){
            E2CRUDMethods.AfterSubmit($el,e);
            $el.removeClass("disabled");
        });
    }
}
/**
* Check the atributes for any before submit actions that we need to execute
*
* Event actions:
* before-save-selector :
* before-save-css :
* before-save-js :
*/
E2CRUDMethods.BeforeSubmit = function($el){
    if(!$el)
        return;
    
    if(E2JSHelpers.hasAttribute($el, "before-save-selector") && E2JSHelpers.hasAttribute($el, "before-save-css")){
        var selector = $el.attr("before-save-selector");
        var css = $el.attr("before-save-css");
        if(css){
            $(selector).removeClass();
			$(selector).addClass(css);
		}
    }
    
    if(E2JSHelpers.hasAttribute($el, "before-save-js")){
        var code = $el.attr("before-save-js");
        window.eval.call(window,'(function (element) {'+code+'})')($el);
    }
    
    if(E2JSHelpers.hasAttribute($el,"error-save-selector") && E2JSHelpers.hasAttribute($el,"error-save-css")){
        var selector = $el.attr("error-save-selector");
        $(selector).removeClass($el.attr("error-save-css"));
    }
}
/**
* Check the atributes for any before submit actions that we need to execute
*
* after-save-selector : 
* after-save-css :
* after-save-js :
* error-save-selector
* error-save-css
*
*@param e {object} server response
*/
E2CRUDMethods.AfterSubmit = function($el,e){
    if(!$el)
        return;
    
    if(E2JSHelpers.hasAttribute($el, "after-save-selector") && E2JSHelpers.hasAttribute($el, "after-save-css")){
        var selector = $el.attr("after-save-selector");
        var css = $el.attr("after-save-css");
        if(css){
			$(selector).removeClass();
			$(selector).addClass(css);
		}
            
    }
    
    if(E2JSHelpers.hasAttribute($el, "after-save-js")){
        var code = $el.attr("after-save-js");
        window.eval.call(window,'(function (element,result) {'+code+'})')($el,e);
    }
    
    if(e && e.error){
        var msg = e.error["message"] || "Unknown error occured.";
        if(E2JSHelpers.hasAttribute($el,"error-save-selector") && E2JSHelpers.hasAttribute($el,"error-save-css")){
            var selector = $el.attr("error-save-selector");
			var css = $el.attr("error-save-css");
			if(css){
				$(selector).addClass(css);
			}
            $el.html(msg);
        }
    }
}
/**
* Calls the create new record method api
*
*@param entity {string}
*@param values {object}
*@param callback {function}
*@param ctx {object}
*/
E2CRUDMethods.CreateNewRecord = function(entity, values, callback, ctx){
    var e2Data = new E2Data();
    
    e2Data.entity = entity;
    e2Data.values = values;
    e2Data.callback = callback;
    e2Data.cbCtx = ctx;
    
    e2Data.CreateData();
}
/**
* Calls the modify record method api
*
*@param entity {string}
*@param values {object}
*@param filter {object}
*@param callback {function}
*@param ctx {object}
*/
E2CRUDMethods.UpdateExistingRecord = function(entity, values, filter, callback, ctx){
    var e2Data = new E2Data();
    
    e2Data.entity = entity;
    e2Data.values = values;
    e2Data.filter = filter;
    e2Data.callback = callback;
    e2Data.cbCtx = ctx;
    
    e2Data.ModifyData();
}
/**
* Attempts to resolve the id for the PG button
*
*@return {string|null}
*/
E2CRUDMethods.__resolvePGButtonFilter = function($el){
    if(!$el)
        return null;
    
    if(E2JSHelpers.hasAttribute($el, "id-hidden-at")){
        var idHidden = $el.attr("id-hidden-at");
        
        if(idHidden){
            var $hidden = $(idHidden);
            if($hidden && $hidden.val()){
                return 'Id="'+$hidden.val()+'"';
            }
        }
    }else if(E2JSHelpers.hasAttribute($el, "id-filter-from-url")){
        var idUrl = $el.attr("id-filter-from-url");
        
        if(idUrl){
            var urlFilter = E2JSHelpers.getUrlParameter("filter");
            if(urlFilter)
                return urlFilter;
        }
    }else if(E2JSHelpers.hasAttribute($el, "id-fixed")){
        var idFixed = $el.attr("id-fixed");
        
        if(idFixed)
            return 'Id="'+idFixed+'"';
    }
    
    return null;
}
/**
* Get/Find the submit values from the inputs. Resolve the fieldname and it's value
*
*@param tableName {string} TableName, provider/TableName
*@param selectors {array} optional, if you input the selectors, we won't search the dom
*
*@return {object} key value pairs. FieldnName : value
*/
E2CRUDMethods.__getInputValues = function(tableName, selectors){
    var ret = {};
    
    if(!tableName)
        return ret;
    
    var _tableName = E2CRUDMethods.__resolveTableName(tableName);
    
    if(!selectors)
        selectors = E2CRUDMethods.__findCompleteSelectorsFromTableName(_tableName);
    
    $.each(selectors, function(index, value){
        $input = $("[idoptag='"+value+"']");
        if(!$input)
            return true;        //continue;
        
        var fieldName = E2CRUDMethods.__resolveFieldName(value);
        
        if(!fieldName)
            return true;
        
        var val = $input.val();
        
        if(!val)
            return true;
        
        
        ret[fieldName] = val;
    });
    
    return ret;
}
/**
* Resolves the actual field name from the idoptag (<!--$DRT_F_dopinus/ServerApiServices/ServerDescription-->)
*
*@return {string|null}
*/
E2CRUDMethods.__resolveFieldName = function(idoptag){   
    if(!idoptag)
        return null;
    
    var trimmed = E2JSHelpers.rtrim(idoptag, "-->");
    
    if(!trimmed)
        return null;
    
    var split = trimmed.split("/");
    
    if(split.length == 0)
        return null;
    
    return split[split.length-1];
}
/**
* Get table name from entity full name
*
*@return {string}
*/
E2CRUDMethods.__resolveTableName = function(entityName){
    var ret = "Unknown";
    
    var split = entityName.split("/");
    
    if(split.length == 0)
        return ret;
    
    return split[split.length-1];
}
/**
* Searches for all idoptag, where this table name is found
*
*@return {array} of absolute selectors
*/
E2CRUDMethods.__findCompleteSelectorsFromTableName = function(tableName){
    var ret = [];
    
    $("[idoptag*='"+tableName+"']").each(function(index, element){
        ret.push($(element).attr("idoptag"));
    });
    
    return ret;
}
