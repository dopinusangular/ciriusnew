import { Component, OnDestroy } from '@angular/core';
import { NotifyChangesService } from '../notify-changes.service';
import { Subscription } from 'rxjs';
import { Student } from '../Student';
declare const qx: any;
declare const com: any;
declare const uil: any;
declare const $: any;

@Component({
  selector: 'app-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.css']
})
export class BasicInfoComponent implements OnDestroy {
  student: Student = new Student(false);
  userSaveDialog = false;
  subscription: Subscription;
  kidsConsets = {};
  constructor(private notfychangeservice: NotifyChangesService) {
    this.subscription = this.notfychangeservice.getCurrentRow().subscribe(rowData => {
      if (rowData) {
        this.student = new Student(rowData);
      } else {
        // clear messages when empty message received
        this.student = new Student(false);
      }
      this.kidsConsets = notfychangeservice.getConsetSetKid();
      this.reloadForm();
    });
    if (this.student.id == null && this.notfychangeservice.getStudentData() != null) {
      this.student = new Student(this.notfychangeservice.getStudentData());
      this.kidsConsets = notfychangeservice.getConsetSetKid();
      this.reloadForm();
    }
  }
  ngOnDestroy() {
    console.log("destroy basic");
    this.subscription.unsubscribe();
    this.student = null;

  }
  reloadForm() {
    console.log(this.student);
  }
  changeModel() {
    //todo check if values are changed
    this.userSaveDialog = true;
  }
  Cancel() {
    this.student.data = JSON.parse(JSON.stringify(this.student.oldData));
    this.userSaveDialog = false;
  }
  Save() {
    const values = this.getChanges();
    this.notfychangeservice.getBrowse().modifySelectedRow(values, (status, error) => {
      if (status == true) {
        this.userSaveDialog = false;
        this.student.oldData = JSON.parse(JSON.stringify(this.student.data));
      }
    }, this);
  }
  getChanges() {
    const modifyObj = {};
    for (const key in this.student.data) {
      if (key == 'Id' || key.includes('E2IF_')||key==='picture') {
        continue;
      }
      if (typeof this.student.data[key] === 'object') {
        continue;
      }
      if (this.student.data[key] != this.student.oldData[key]) {
        modifyObj[key] = this.student.data[key];
      }
    }
    console.log(modifyObj);
    return modifyObj;
  }
  imageUpload() {
    console.log('Image Upload');
    qx.io.PartLoader.require(['UploadMgr'], function () {
      const dialog = new qx.ui.window.Window('Izberi sliko za nalaganje...', '').set({
        width: 300, height: 200, showMaximize: false, showMinimize: false, movable: false
      });

      const layout_dlg = new qx.ui.layout.Grow();
      const that = this;

      dialog.set({
        layout: layout_dlg,
        modal: true
      });

      dialog.center();

      if (this.controlUploadFinished) {
        this.fileState = '';
        this.docUploaded = '';

        dialog.addListener('beforeClose', function (evt) {
          if ((this.fileState != '' && this.fileState != 'uploaded') && (this.docUploaded != '' && this.docUploaded != 'finished')) {
            evt.stop();
            new uil.misc.AlertWindow('Napaka', 'Nalaganje še ni zaključeno!', [{
              'text': 'Ok'
            }
            ]);
          }
        });
      }


      dialog.open();

      // Create a container for the grid
      const layout_cnt = new qx.ui.layout.Grid();
      const container = new qx.ui.container.Composite(layout_cnt);

      const btn = new com.zenesis.qx.upload.UploadButton('Izberi sliko', 'cm1/test.png');
      const url = this.notfychangeservice.getBaseUrl() + 'File';


      const uploader = new com.zenesis.qx.upload.UploadMgr(btn, url, true, '');


      const lst = new qx.ui.form.List();

      // Create addFile event handler responsible for actual file upload and data create
      uploader.addListener('addFile', function (evt) {
        that.__createAddFileEventHandler(evt, lst,dialog);
      }, this);

      lst.set({
        width: 300
      });

      container.add(lst, {
        row: 0,
        column: 0
      });
      container.add(btn, {
        row: 1,
        column: 0
      });
      dialog.add(container);

    }, this);
  }
  __createAddFileEventHandler(evt, lst,dialog) {
    const file = evt.getData();
    const item = new qx.ui.form.ListItem(file.getFilename() + ' (queued for upload)', null, file);
    const that = this;

    lst.add(item);

    // On modern browsers (ie not IE) we will get progress updates
    const progressListenerId = file.addListener('changeProgress', (evt) => {
      item.setLabel(file.getFilename() + ': ' + evt.getData() + ' / ' + file.getSize() + ' - ' +
        Math.round(evt.getData() / file.getSize() * 100) + '%');

      //this.docUploaded = file.getProgress();
    }, this);

    // All browsers can at least get changes in state (ie "uploading", "cancelled", and "uploaded")
    const stateListenerId = file.addListener('changeState', (evt) => {
      that.__createChangeStateEventHandler(evt, item, file, progressListenerId, stateListenerId,dialog);
    }, this);
  }
  __createChangeStateEventHandler(evt, item, file, progressListenerId, stateListenerId,dialog) {
    const state = evt.getData();


    if (state === 'uploading') {
      item.setLabel(file.getFilename() + ' (Uploading...)');
    } else if (state === 'uploaded') {
      item.setLabel(file.getFilename() + ' (Complete)');
    } else if (state === 'cancelled') {
      item.setLabel(file.getFilename() + ' Server Error)');
    }

    if (state === 'cancelled') {
      file.removeListenerById(stateListenerId);
      file.removeListenerById(progressListenerId);
    } else if (state === 'uploaded') {
      file.removeListenerById(stateListenerId);
      file.removeListenerById(progressListenerId);
      const fileId = file.getId();
      this.student.data['Slika']='File/' + fileId;
      this.student.data['Picture']=this.notfychangeservice.getBaseUrl() + 'File/' + fileId;
      this.userSaveDialog=true;
    }
  }
}
