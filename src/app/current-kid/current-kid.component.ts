import { Component, OnInit } from '@angular/core';
import { NotifyChangesService } from '../notify-changes.service';
import { Subscription } from 'rxjs';
import { Student } from '../Student';
declare const E2Browse: any;
declare const E2Auth: any;
declare const RpcClient: any;
declare const uil: any;
declare const qx: any;
@Component({
  selector: 'app-current-kid',
  templateUrl: './current-kid.component.html',
  styleUrls: ['./current-kid.component.css']
})
export class CurrentKidComponent implements OnInit {
  student: Student = new Student(false);
  userSaveDialog = false;
  subscription: Subscription;
  constructor(private notfychangeservice: NotifyChangesService) {
    this.subscription = this.notfychangeservice.getCurrentRow().subscribe(rowData => {
      if (rowData) {
        this.student = new Student(rowData);
      } else {
        // clear messages when empty message received
        this.student = new Student(false);
      }
    });
  }

  ngOnInit() {
    console.log('qxApplicationReadyInit')
    const that = this;
    if (this.notfychangeservice.isUilLoaded()) {
      that.GetSessionInfo();
    } else {
      window.addEventListener('qxApplicationReady', () => {
        console.log('qxApplicationReadyCurrent');

        that.GetSessionInfo();
      }, true);
    }
  }
  prepareBrowse(info: any) {
    qx.io.PartLoader.require(['Table', 'Document'], () => {
      this.loadBrowse(info);
  }, this);
  }
  loadBrowse(info: any) {
    const e2Browse = new E2Browse();
    e2Browse.name = 'kids';
    e2Browse.componentInfo = info;
    e2Browse.tableInfo = this.getMainTableInfo(info);
    e2Browse.tableMetaData = this.getMainTableMetaData(info);
    e2Browse.resizeContainerId = 'kidsListResize';
    //e2Browse.resizeHeightId = 'kidsListForm';
    e2Browse.inlineContainerId = 'kidsListBrowse';
    console.log(e2Browse);
    e2Browse.build();
    e2Browse.show();
    this.notfychangeservice.setBrowse(e2Browse);
    qx.locale.Manager.getInstance().setLocale('sl_SI');
    qx.theme.manager.Meta.getInstance().setTheme(qx.Theme.getByName('uil.theme.IndigoTheme'));
  }
  getMainTableInfo(info) {
    if (!info || !info.data || info.data.length === 0) {
      return null;
    }
    for (let i = 0; i < info.data.length; i++) {
      if (info.data[i].entity && info.data[i].entity.indexOf('CK_UcenciDijaki') > -1) {
        return info.data[i];
      }
    }
    return null;
  }
  getMainTableMetaData(info) {
    if (!info || !info.subComponents) {
      return null;
    }
    if (info.subComponents['e6224235-f160-11e8-8102-bbd1cba9302c']) {
      return info.subComponents['e6224235-f160-11e8-8102-bbd1cba9302c'];
    }

    return null;
  }

  GetSessionInfo() {
    const that = this;
    const sess = new E2Auth();

    sess.callback = (ctx, data) => {
      console.log(data);
      if (data.result) {
        // tslint:disable-next-line: variable-name
        const _sessionStorage = uil.core.SessionStorage.getInstance();
        _sessionStorage._storeSessionData(data.result);
        that.getComponentInfo();
      }
    };
    sess.GetSessionInfo();
  }
  getComponentInfo() {
    const req = {
      method: 'Component.GetComponentInfo',
      params: { component: 'test/Ciri' }
    };
    uil.misc.Generic.sendRequest(req, (data) => {
      if (data.result) {
        this.prepareBrowse(data.result);
      }
    }, this);
  }
  PreviousRecord() {
    this.notfychangeservice.previousRecord();
  }
  NextRecord() {
    this.notfychangeservice.nextRecord();
  }
}
