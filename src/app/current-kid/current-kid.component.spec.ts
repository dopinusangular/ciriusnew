import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentKidComponent } from './current-kid.component';

describe('CurrentKidComponent', () => {
  let component: CurrentKidComponent;
  let fixture: ComponentFixture<CurrentKidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentKidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentKidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
