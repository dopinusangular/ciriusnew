import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotifyChangesService } from '../notify-changes.service';
import { Subscription } from 'rxjs';
import { Student } from '../Student';

declare const E2Extension: any;

@Component({
  selector: 'app-kids-personaliz',
  templateUrl: './kids-personaliz.component.html',
  styleUrls: ['./kids-personaliz.component.css']
})
export class KidsPersonalizComponent implements OnDestroy {
  student: Student = new Student(false);
  userSaveDialog = false;
  subscription: Subscription;
  loadedAdjust = null;
  adjustCode = {};
  adjustCodeObj = {};
  kidAdjust = {};
  kidAdjustOld = {};
  additonal = {};
  additonalText = {}
  additonalTextCMB = {};
  additonalTextCMBSelected = {};
  showAdjust = false;
  constructor(private notfychangeservice: NotifyChangesService) {
    this.subscription = this.notfychangeservice.getCurrentRow().subscribe(rowData => {
      if (rowData) {
        this.student = new Student(rowData);

      } else {
        // clear messages when empty message received
        this.student = new Student(false);
      }
      this.reloadForm();
    });
    if (this.student.id === null && this.notfychangeservice.getStudentData() !== null) {
      this.student = new Student(this.notfychangeservice.getStudentData());
      this.reloadForm();
    }
  }

  ngOnDestroy() {
    console.log("destroy personalize");
    this.subscription.unsubscribe();
    this.kidAdjust = null;
    this.kidAdjustOld = null;
    this.student = null;

  }
  reloadForm() {
    if (!this.student.id) {
      this.kidAdjust = {};
      this.kidAdjust = {};
      return;
    }
    if (this.loadedAdjust === false) {
      return;
    }
    if (this.loadedAdjust === null) {
      this.loadedAdjust = false;
      this.notfychangeservice.loadAdjust((data, dataObj) => {
        this.adjustCode = data;
        this.adjustCodeObj = dataObj;
        this.loadedAdjust = true;
        this.reloadForm();
        return;
      });

      return;
    }
    const e2Ext = new E2Extension();
    e2Ext.extension = 'CiriusAdjustments';
    e2Ext.args = { requestType: 'get', student: this.student.id };
    e2Ext.callback = (e) => {
      if (e.result) {
        this._loadData(e.result);
      }
    };
    e2Ext.cbCtx = this;
    e2Ext.Execute();
  }
  _loadData(data) {
    this.kidAdjustOld = JSON.parse(JSON.stringify(data));
    this.kidAdjust = data;
    this.additonalText = data["__aditonalData"];
    this.userSaveDialog = false;
    for (const key in this.adjustCodeObj) {
      this.changeVisibilityAdditonal(key, true);
    }
    console.log(this.additonalTextCMB);
    console.log(this.additonalTextCMBSelected);

  }
  changeVisibilityAdditonal(key, iniSet) {
    let visibleAdditonal = false;
    const array = [];
    if (!this.kidAdjust[key]) {

      this.additonalTextCMB[key] = array;
      this.additonalTextCMBSelected[key] = {};
      this.additonal[key] = false;

      return;
    }
    for (let i = 0; i < this.kidAdjust[key].length; i++) {

      if (this.adjustCodeObj[key][this.kidAdjust[key][i]].Additonal == true) {
        visibleAdditonal = true;
        //todo check if exist
        array.push({
          "Id": this.adjustCodeObj[key][this.kidAdjust[key][i]].Id,
          "Title": this.adjustCodeObj[key][this.kidAdjust[key][i]].Title
        });
      }
    }



    if (this.additonal[key] != visibleAdditonal) {
      this.additonal[key] = visibleAdditonal;
    }
    if (array.length > 0) {
      this.additonalTextCMBSelected[key] = array[0];
    }
    this.additonalTextCMB[key] = array;
    if (!iniSet) {
      this.userSaveDialog = true;
    }


  }
  changeModel(value) {
    console.log(value);
    if (value) {
      console.log(value);
      this.changeVisibilityAdditonal(value, false);

    }
    else {
      console.log("aaaa");
      this.userSaveDialog = true;
    }


  }
  identify(index, item) {
    return item.name;
  }
  changeText() {
    this.userSaveDialog = true;
  }
  CancelUserOnClick() {
    this._loadData(this.kidAdjustOld);
  }
  SaveOrAddUserOnClick() {
    const obj = this.kidAdjust;
    obj['__aditonalData'] = this.additonalText;
    const e2Ext = new E2Extension();
    e2Ext.extension = 'CiriusAdjustments';
    e2Ext.args = { requestType: 'sync', student: this.student.id, syncObj: obj };
    e2Ext.cbCtx = this;
    e2Ext.callback = (e) => {
      if (e.result) {
        this._loadData(obj);
        this.notfychangeservice.pushMessage('Opozorilo', 'Shranjevanje je uspešno.');
      }
      else {
        this.notfychangeservice.pushMessage('Opozorilo', 'Shranjevanje ni bilo uspešno. Error:' + e.error);
      }
    };
    e2Ext.Execute();
  }
  changeTextAdd(value) {

  }
}
