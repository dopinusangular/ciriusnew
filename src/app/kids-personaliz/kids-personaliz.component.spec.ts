import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KidsPersonalizComponent } from './kids-personaliz.component';

describe('KidsPersonalizComponent', () => {
  let component: KidsPersonalizComponent;
  let fixture: ComponentFixture<KidsPersonalizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KidsPersonalizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KidsPersonalizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
