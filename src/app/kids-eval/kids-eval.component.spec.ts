import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KidsEvalComponent } from './kids-eval.component';

describe('KidsEvalComponent', () => {
  let component: KidsEvalComponent;
  let fixture: ComponentFixture<KidsEvalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KidsEvalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KidsEvalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
