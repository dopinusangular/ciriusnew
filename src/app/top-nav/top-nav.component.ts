import { Component, OnInit } from '@angular/core';
import { NotifyChangesService } from '../notify-changes.service';
import { Subscription } from 'rxjs';
declare const $: any;
declare const E2Auth: any;
@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent implements OnInit {

  subscription: Subscription;
  constructor(private notfychangeservice: NotifyChangesService) {

  }

  ngOnInit() {

    $('#side-menu').metisMenu({
      preventDefault: false, // mogoc bo delal na hover
      parentTrigger: '.nav-item',
      triggerElement: '.nav-link'
    });

  }
  Logout() {
    this.notfychangeservice.signOut();
  }

}
