import { Component } from '@angular/core';
import { NotifyChangesService } from './notify-changes.service';
import { Subscription } from 'rxjs';

declare const E2Auth: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Cirius';
  isLogin=null;
  subscription: Subscription;
  constructor(private notfychangeservice: NotifyChangesService){
    this.subscription = this.notfychangeservice.changeStatusOfLogin().subscribe(loginStatus => {
      console.log(loginStatus)
      this.isLogin=loginStatus;

    });


    this.notfychangeservice.IsAuth();
  }

}
