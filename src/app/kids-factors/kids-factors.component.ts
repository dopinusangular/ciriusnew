import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotifyChangesService } from '../notify-changes.service';
import { Subscription } from 'rxjs';
import { Student } from '../Student';

declare const E2Data: any;
@Component({
  selector: 'app-kids-factors',
  templateUrl: './kids-factors.component.html',
  styleUrls: ['./kids-factors.component.css']
})
export class KidsFactorsComponent implements OnDestroy {
  student: Student = new Student(false);
  userSaveDialog = false;
  subscription: Subscription;
  factors = {
    Pod_Ods_Stal: [{ Id: null, Priimekinime: 'Ni izbran' }],
    Eko_Biv_Polozaj: [{ Id: null, Priimekinime: 'Ni izbran' }]
  };
  saveButton = { EBP: 'Spremeni', POS: 'Spremeni' };
  editMode = { EBP: false, POS: false };
  kidFactor = { Pod_Ods_Stal: null, Eko_Biv_Polozaj: null };
  kidFactorOld = {};
  loadedFactor = null;
  constructor(private notfychangeservice: NotifyChangesService) {
    this.subscription = this.notfychangeservice.getCurrentRow().subscribe(rowData => {
      if (rowData) {
        this.student = new Student(rowData);

      } else {
        // clear messages when empty message received
        this.student = new Student(false);
      }
      this.reloadForm();
    });
    if (this.student.id == null && this.notfychangeservice.getStudentData() != null) {
      this.student = new Student(this.notfychangeservice.getStudentData());
      this.reloadForm();
    }
  }
  reloadForm() {
    if (!this.student.id) {
      this.kidFactor = { Pod_Ods_Stal: null, Eko_Biv_Polozaj: null };
      this.kidFactorOld = {};
      return;
    }
    if (this.loadedFactor === false) {
      return;
    }
    if (this.loadedFactor === null) {
      this.loadedFactor = false;
      this.notfychangeservice.loadFactor((data) => {
        this.factors = data;
        console.log(data);
        this.loadedFactor = true;
        this.reloadForm();
      });

      return;
    }
    const e2Data = new E2Data();
    e2Data.entity = 'CK_UcenecOkolskiDejavniki';
    e2Data.callback = (e) => {
      if (e.result && e.result.data.length > 0) {
        this.setFormData(e.result.data[0]);
      } else {
        this.setFormData({ Pod_Ods_Stal: null, Eko_Biv_Polozaj: null });
      }

    };
    e2Data.cbCtx = this;
    e2Data.filter = 'UcenecMC="' + this.student.id + '"';

    e2Data.GetData();
  }
  setFormData(data) {
    this.kidFactor = data;
    this.kidFactorOld = JSON.parse(JSON.stringify(data));
    this.userSaveDialog = false;

  }
  ngOnDestroy() {
    console.log('destroy factor');
    this.subscription.unsubscribe();
    this.kidFactor = null;
    this.kidFactorOld = null;
    this.student = null;

  }
  changeFactor() {
    this.userSaveDialog = true;
  }
  cancelPorocilo(value) {
    this.editMode[value] = false;
    this.saveButton[value] = 'Spremeni';
    this.kidFactor[value] = this.kidFactorOld[value];
  }
  savePorocilo(value) {
    if (this.editMode[value] === true) {
      this.editMode[value] = false;
      this.saveButton[value] = 'Spremeni';
      this.SaveOrAddUserOnClick(true, value + '_Porocilo');
    } else {
      this.editMode[value] = true;
      this.saveButton[value] = 'Shrani';
    }
  }
  SaveOrAddUserOnClick(partial, key) {
    if (this.student.id) {
      const e2Data = new E2Data();
      e2Data.entity = 'CK_UcenecOkolskiDejavniki';
      e2Data.cbCtx = this;
      let obj = {};
      if (partial) {
        obj[key] = this.kidFactor[key];
      } else {
        obj = this.sanitazeValues(this.kidFactor, this.kidFactorOld);
      }
      obj['UcenecMC'] = this.student.id;
      e2Data.values = obj;
      let modify = false;
      if (this.kidFactor['Id']) {
        modify = true;
      }
      if (modify) {

      }
      e2Data.callback = (response) => {
        if (response.result) {
          if (!modify) {
            this.kidFactor['Id'] = response.result.values[0]['id'];
            this.kidFactorOld['Id'] = response.result.values[0]['id'];
          }
          if (key) {
            this._SaveCurrentState(this.kidFactor, key);
          } else {
            this.setFormData(this.kidFactor);
            this.userSaveDialog = false;
          }
          /*_SaveCurrentState($ctrl.factorSelectedRowData, key);
          return e2ShowWarning("Opozorilo", "Shranjevanje je uspešno.");*/
        }
        else {
          // return e2ShowWarning("Napaka", "Shranjevanje ni bilo uspešno.");
        }

      }

      if (modify) {
        e2Data.filter = 'Id="' + this.kidFactor['Id'] + '"';
        e2Data.ModifyData();
      }
      else {
        e2Data.CreateData();
      }



    }

  }
  CancelUserOnClick() {
    this.setFormData(this.kidFactorOld);
  }
  _SaveCurrentState(data, key) {
    if (data != null) {
      if (!key) {
        this.kidFactorOld = JSON.parse(JSON.stringify(data));
      }
      else {
        this.kidFactorOld[key] = data[key];
      }
    }
    else {
      this.kidFactorOld = {};
    }

  }
  sanitazeValues(values, savedObj) {
    const modifyObj = {};
    for (const key in values) {
      if (key == 'Id' || key.includes('E2IF_')) {
        continue;
      }
      if (typeof values[key] === 'object') {
        continue;
      }
      if (savedObj != null) {
        let value = values[key];
        if (value == '') {
          value = null;
        }
        if (value == null && !savedObj[key]) {
          continue;
        }
        if (value != savedObj[key]) {
          modifyObj[key] = values[key];
        }
      }
      else {
        modifyObj[key] = values[key]
      }
    }
    return modifyObj;
  }

}

