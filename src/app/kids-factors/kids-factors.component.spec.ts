import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KidsFactorsComponent } from './kids-factors.component';

describe('KidsFactorsComponent', () => {
  let component: KidsFactorsComponent;
  let fixture: ComponentFixture<KidsFactorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KidsFactorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KidsFactorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
