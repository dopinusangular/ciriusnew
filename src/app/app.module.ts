import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { KidsListComponent } from './kids-list/kids-list.component';
import { CurrentKidComponent } from './current-kid/current-kid.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { BasicInfoComponent } from './basic-info/basic-info.component';
import { FormsModule } from '@angular/forms';
import { TeamPanelComponent } from './team-panel/team-panel.component';
import { KidsFactorsComponent } from './kids-factors/kids-factors.component';
import { KidsFunctionComponent } from './kids-function/kids-function.component';
import { KidsPersonalizComponent } from './kids-personaliz/kids-personaliz.component';
import { KidsGoalsComponent } from './kids-goals/kids-goals.component';
import { KidsIpComponent } from './kids-ip/kids-ip.component';
import { KidsEvalComponent } from './kids-eval/kids-eval.component';
import { ContactsLiComponent } from './contacts-li/contacts-li.component';
import { LoginComponent } from './login/login.component';

const appRoutes: Routes = [
  { path: 'cirius/list', component:KidsListComponent },
  { path: 'cirius/factors', component:KidsFactorsComponent },
  { path: 'cirius/function',      component: KidsFunctionComponent },
  { path: 'cirius/personaliz',      component: KidsPersonalizComponent },
  { path: 'cirius/goals',      component: KidsGoalsComponent },
  { path: 'cirius/eval',      component: KidsEvalComponent },
  { path: 'cirius/ip',      component: KidsIpComponent },
  { path: 'cirius',
    redirectTo: '/cirius/list',
    pathMatch: 'full'
  }

];

@NgModule({
  declarations: [
    AppComponent,
    KidsListComponent,
    CurrentKidComponent,
    TopNavComponent,
    BasicInfoComponent,
    TeamPanelComponent,
    KidsFactorsComponent,
    KidsFunctionComponent,
    KidsPersonalizComponent,
    KidsGoalsComponent,
    KidsIpComponent,
    KidsEvalComponent,
    ContactsLiComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
