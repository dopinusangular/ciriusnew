import { Component, OnDestroy } from '@angular/core';
import { NotifyChangesService } from '../notify-changes.service';
import { Subscription } from 'rxjs';
import { Student } from '../Student';

declare const E2Data: any;
@Component({
  selector: 'app-contacts-li',
  templateUrl: './contacts-li.component.html',
  styleUrls: ['./contacts-li.component.css']
})
export class ContactsLiComponent implements OnDestroy {
  student: Student = new Student(false);
  subscription: Subscription;


  kidCoantact = [];

  userSaveDialogTransport = false;
  constructor(private notfychangeservice: NotifyChangesService) {
    this.subscription = this.notfychangeservice.getCurrentRow().subscribe(rowData => {
      if (rowData) {
        this.student = new Student(rowData);

      } else {
        // clear messages when empty message received
        this.student = new Student(false);
      }
      this.reloadForm();
    });
    if (this.student.id === null && this.notfychangeservice.getStudentData() !== null) {
      this.student = new Student(this.notfychangeservice.getStudentData());
      this.reloadForm();
    }
  }

  ngOnDestroy() {
    console.log('destroy kidContact');
    this.subscription.unsubscribe();
    this.student = null;

  }
  reloadForm() {
    if (this.student.id === null) {
      this.kidCoantact = [];
    }
    const data=new E2Data();
    data.entity='CK_KontaktneOsebe';
    data.filter='UcenecMC = "'+this.student.id+'"';
    data.cbCtx=this;
    data.callback=(result)=> {
      if(result&&result.result)
      {
        this.kidCoantact=result.result.data;
      }
    }
    data.GetData();

  }

}
