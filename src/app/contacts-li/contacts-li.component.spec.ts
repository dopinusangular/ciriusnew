import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactsLiComponent } from './contacts-li.component';

describe('ContactsLiComponent', () => {
  let component: ContactsLiComponent;
  let fixture: ComponentFixture<ContactsLiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactsLiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactsLiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
