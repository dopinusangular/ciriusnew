import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotifyChangesService } from '../notify-changes.service';
import { Subscription } from 'rxjs';
import { Student } from '../Student';
declare const E2Extension: any;
@Component({
  selector: 'app-kids-goals',
  templateUrl: './kids-goals.component.html',
  styleUrls: ['./kids-goals.component.css']
})
export class KidsGoalsComponent implements OnDestroy {

  student: Student = new Student(false);
  userSaveDialog = { Pomembnost: false, Zastavljeni: false, Dosezeni: false, SodStar: false, SodMlad: false };
  subscription: Subscription;
  loadedGoals = null;
  GoalCode = { Pomembnost: [] };

  saveButton = { Pomembnost: 'Spremeni', Zastavljeni: 'Spremeni', Dosezeni: 'Spremeni', SodStar: 'Spremeni', SodMlad: 'Spremeni' };
  kidGoals = { Pomembnost: {}, Zastavljeni: {}, Dosezeni: {}, SodStar: {}, SodMlad: {} };
  kidGoalsOld = {};

  editMode = { Pomembnost: false, Zastavljeni: false, Dosezeni: false, SodStar: false, SodMlad: false };
  constructor(private notfychangeservice: NotifyChangesService) {
    this.subscription = this.notfychangeservice.getCurrentRow().subscribe(rowData => {
      if (rowData) {
        this.student = new Student(rowData);

      } else {
        // clear messages when empty message received
        this.student = new Student(false);
      }
      this.reloadForm();
    });
    if (this.student.id == null && this.notfychangeservice.getStudentData() != null) {
      this.student = new Student(this.notfychangeservice.getStudentData());
      this.reloadForm();
    }
  }

  ngOnDestroy() {
    console.log("destroy Goals");
    this.subscription.unsubscribe();
    this.kidGoals = null;
    this.kidGoalsOld = null;
    this.student=null;

  }
  reloadForm() {
    if (!this.student.id) {
      this.kidGoals = { Pomembnost: {}, Zastavljeni: {}, Dosezeni: {}, SodStar: {}, SodMlad: {} };
      this.kidGoalsOld = {};
      return;
    }
    if (this.loadedGoals === false) {
      return;
    }
    if (this.loadedGoals === null) {
      this.loadedGoals = false;
      this.notfychangeservice.loadGoals((data) => {
        this.GoalCode = data;
        this.loadedGoals = true;
        this.reloadForm();
      });

      return;
    }
    const e2Ext = new E2Extension();
    e2Ext.extension = 'CiriusGoalsExten';
    e2Ext.args = { type: 'get', student: this.student.id };
    e2Ext.callback = (e) => {
      if (e.result) {
        this._loadData(e.result);
      }
    };
    e2Ext.cbCtx = this;
    e2Ext.Execute();
  }
  _loadData(data) {
    this.kidGoalsOld = JSON.parse(JSON.stringify(data));
    this.kidGoals = data;
    this.userSaveDialog = { Pomembnost: false, Zastavljeni: false, Dosezeni: false, SodStar: false, SodMlad: false };
  }
  clickPorocilo(value) {
    if (this.editMode[value] == true) {
      this.editMode[value] = false;
      this.saveButton[value] = "Spremeni";
      this.SaveOrAddUserOnClick(true, value, "Porocilo");
    }
    else {
      this.editMode[value] = true;
      this.saveButton[value] = "Shrani";
    }
  }
  cancelPorocilo(value) {
    this.editMode[value] = false;
    this.saveButton[value] = 'Spremeni';
    this.kidGoals[value] = this.kidGoalsOld[value];
  }
  SaveOrAddUserOnClick = function (partial, key, subkey) {
    if (this.student.id) {
      const e2ext = new E2Extension();

      e2ext.extension = 'CiriusGoalsExten';
      e2ext.cbCtx = this;
      e2ext.args = { type: 'sync', student: this.student.id, group: key };

      const obj = this.notfychangeservice.sanitazeValues(this.kidGoals[key], this.kidGoalsOld[key]);

      if (partial) {
        e2ext.args['syncObj'] = {};
        e2ext.args['syncObj'][subkey] = obj[subkey];
      }
      else {
        e2ext.args['syncObj'] = obj;
      }

      e2ext.callback = (e) => {
        if (e.result) {
          this._SaveCurrentState(this.kidGoals, key);
          if (!partial) {
            this.userSaveDialog[key] = false;
          }
          return this.notfychangeservice.pushMessage('Opozorilo', 'Shranjevanje je uspešno.');
        }
        else {
          return this.notfychangeservice.pushMessage('Napaka', 'Shranjevanje ni bilo uspešno.');
        }

      }
      e2ext.cbCtx = this;
      e2ext.Execute();



    }
    this.saveButton[key] = 'Spremeni';
  }
  SaveOnClick(key) {

    this.SaveOrAddUserOnClick(false, key, false);
  }
  _SaveCurrentState(data, key, subkey) {
    if (data != null) {
      if (!key) {
        this.kidGoalsOld = JSON.parse(JSON.stringify(data));
      }
      else {
        if (subkey) {
          this.kidGoalsOld[key][subkey] = data[key][subkey];
        }
        else {
          this.kidGoalsOld[key] = JSON.parse(JSON.stringify(data[key]));
        }
      }
    }
    else {
      this.kidGoalsOld = {};
    }

    if (!key) {
      this.saveButton[key] = 'Spremeni';
    }
  }
  changeModel(value) {
    this.userSaveDialog[value] = true;
  }
  changeTextModel(value) {

  }

}
