import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KidsGoalsComponent } from './kids-goals.component';

describe('KidsGoalsComponent', () => {
  let component: KidsGoalsComponent;
  let fixture: ComponentFixture<KidsGoalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KidsGoalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KidsGoalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
