export class Student {
  id: string;
  data: object;
  oldData: object;
  constructor(rowData) {
    if (rowData) {
      this.id = rowData['Id'];
      this.data = rowData;
      this.oldData = JSON.parse(JSON.stringify(rowData));
    }
    else {
      this.id = null;
      this.data = {};
      this.oldData = {};
    }
  }
}
