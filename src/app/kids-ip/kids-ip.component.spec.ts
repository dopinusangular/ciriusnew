import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KidsIpComponent } from './kids-ip.component';

describe('KidsIpComponent', () => {
  let component: KidsIpComponent;
  let fixture: ComponentFixture<KidsIpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KidsIpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KidsIpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
