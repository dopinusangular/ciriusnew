import { TestBed } from '@angular/core/testing';

import { NotifyChangesService } from './notify-changes.service';

describe('NotifyChangesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotifyChangesService = TestBed.get(NotifyChangesService);
    expect(service).toBeTruthy();
  });
});
