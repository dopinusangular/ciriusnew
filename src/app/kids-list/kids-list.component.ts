import { Component, OnDestroy} from '@angular/core';
import { NotifyChangesService } from '../notify-changes.service';
import { Subscription } from 'rxjs';
import { Student } from '../Student';

declare const E2Extension: any;
@Component({
  selector: 'app-kids-list',
  templateUrl: './kids-list.component.html',
  styleUrls: ['./kids-list.component.css']
})
export class KidsListComponent implements OnDestroy {
  student: Student = new Student(false);
  subscription: Subscription;
  e2ExtTransport = null;
  transportConstset = {};
  kidtransport = {};
  kidtransportOld = {};
  userSaveDialogTransport = false;
  constructor(private notfychangeservice: NotifyChangesService) {
    this.subscription = this.notfychangeservice.getCurrentRow().subscribe(rowData => {
      if (rowData) {
        this.student = new Student(rowData);

      } else {
        // clear messages when empty message received
        this.student = new Student(false);
      }
      this.reloadForm();
    });
    if(this.student.id==null&&this.notfychangeservice.getStudentData()!=null)
    {
      this.student = new Student( this.notfychangeservice.getStudentData());
      this.reloadForm();
    }
  }

  ngOnDestroy() {
    console.log('destroy kidList');
    this.subscription.unsubscribe();

    this.student = null;

  }
  reloadForm() {
    const that = this;

    this.notfychangeservice.loadTransportConset((constsets) => {
      that.transportConstset = constsets;
      console.log(that.transportConstset);
      that.getTrasnportOnKid();
    });

  }
  getTrasnportOnKid() {
    if (!this.student.id) {
      return;
    }

    if (this.e2ExtTransport == null) {
      this.e2ExtTransport = new E2Extension();
      this.e2ExtTransport.extension = 'CiriusTransportExten';

      this.e2ExtTransport.cbCtx = this;
      this.e2ExtTransport.callback = (e) => {
        if (e && e.result ) {
          this.loadTransport(e.result);
        }  else {
        }

      };
    }
    this.e2ExtTransport.args={type:'get',student:this.student.id};
    this.e2ExtTransport.Execute();
  }
  loadTransport(data) {
    this.kidtransport=data;
    this.kidtransportOld=JSON.parse(JSON.stringify(data));
    this.userSaveDialogTransport=false;
  }
  SaveTransport() {

    const e2CreateOrModify = new E2Extension();


    e2CreateOrModify.extension = 'CK_UcenciPrevozi';
    e2CreateOrModify.cbCtx = this;
    e2CreateOrModify.args={student:this.student.id,syncObj:this.kidtransport,type:'sync'};

    e2CreateOrModify.callback = (response) => {
      console.log(response);
      if (response.result) {
        this.notfychangeservice.pushMessage('Opozorilo', 'Shranjevanje je bilo uspešno.');
        this.userSaveDialogTransport = false;
      } else {
        this.notfychangeservice.pushMessage('Napaka', 'Shranjevanje ni bilo uspešno! Napaka:'+response.error);
      }
    };
    e2CreateOrModify.Execute();

  }
  CancelTransport() {
    this.kidtransport = JSON.parse(JSON.stringify(this.kidtransportOld));
    this.userSaveDialogTransport = false;
  }
  changeTransport() {
    this.userSaveDialogTransport = true;
  }
  getChanges(data, dataOld) {
    const modifyObj = {};
    for (const key in data) {
      if (key == 'Id' || key.includes('E2IF_')) {
        continue;
      }
      if (typeof data[key] === 'object') {
        continue;
      }
      if (data[key] != dataOld[key]) {
        modifyObj[key] = data[key];
      }
    }

    return modifyObj;
  }


}
