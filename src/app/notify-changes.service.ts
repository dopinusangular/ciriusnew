import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
declare const E2Data: any;
declare const E2Extension: any;
declare const E2Auth: any;
declare const uil: any;
declare const $: any;
declare const RpcClient: any;
declare const E2Browse: any;
@Injectable({
  providedIn: 'root'
})
export class NotifyChangesService {
   private baseUrl = 'https://dev.dopinus.com/aljaz/';
  //private baseUrl = '/';

  private uilIsLoaded = false;
  private browse = null;
  private currentRowData = new Subject<any>();
  private changeLogin = new Subject<any>();
  private currentRow = {};

  private loadedConstSets = null;
  private consetKid = null;
  private consetTransport = null;
  private teamMembers = null;
  private factorConstSet = null;
  private functionCode = null;
  private adjustCode = null;
  private adjustCodeObj = null;
  private adjustCodeIP = null;
  private adjustCodeIPObj = null;
  private goalsCode = null;


  constructor() {
    RpcClient.config = {};
    RpcClient.config.url = this.getBaseUrl() + 'json.rpc';
    RpcClient.config.cors = true;
    window.addEventListener('qxApplicationReady', () => {
      this.uilIsLoaded = true;
      console.log('qxApplicationReadyService');
      uil.core.config.jsonRpcUrl = this.getBaseUrl() + 'json.rpc';
      uil.core.config.baseUrl = this.getBaseUrl();
    }, true);
  }
  getBaseUrl() {
    return this.baseUrl;
  }
  // autehenticate functions
  signOut() {
    const auth = new E2Auth();

    auth.callback = () => {
      this.changeLogin.next(false);
    };
    auth.cbCtx = this;
    auth.SignOut();
  }
  IsAuth() {
    const e2Auth = new E2Auth();
    e2Auth.callback = (ctx, isAuth: boolean) => {
      this.changingStatusOfLogin(isAuth);
    };
    e2Auth.cbCtx = this;
    e2Auth.IsAuth();
  }
  authenticate(username: string, password: string) {
    const auth = new E2Auth();
    auth.username = username;
    auth.password = password;
    auth.callback = (authStatus: boolean) => {
      if (authStatus === true) {
        this.pushMessage('Obvestilo', 'Prijava je bila uspešna.');
      } else {
        this.pushMessage('Napaka', 'Prijava je bila neuspešna!');
      }
      this.changingStatusOfLogin(authStatus);
    }
    auth.cbCtx = this;
    auth.Authenticate();
  }
  changeStatusOfLogin() {
    return this.changeLogin.asObservable();
  }
  private changingStatusOfLogin(authStatus: boolean) {
    this.changeLogin.next(authStatus);
    if (authStatus === true && this.loadedConstSets === null) {
      this.loadedConstSets = false;
      this.loadConstSets();
    }
  }
  //browse,qx and change row section
  getCurrentRow() {
    return this.currentRowData.asObservable();
  }
  isUilLoaded() {
    return this.uilIsLoaded;
  }
  getBrowse() {
    return this.browse;
  }
  setBrowse(browse: any) {
    this.browse = browse;
    this.__attachListeners();
    this.loadConstKidSet();
  }
  previousRecord() {
    this.browse.getBrowse().getTable().fireDataEvent('previousRecord');
  }
  nextRecord() {
    this.browse.getBrowse().getTable().fireDataEvent('nextRecord');
  }
  __attachListeners() {

    this.browse.getBrowse().getTable().getSelectionModel().addListener('changeSelection', function (e) {
      let data = null;
      data = this.browse.getSelectedRowData();
      if (jQuery.isEmptyObject(data)) {
        const ranges = this.browse.getBrowse().getTable().getSelectionModel().getSelectedRanges();
        if (ranges.length > 0) {
          data = this.browse.getBrowse().getTable().getTableModel().getRowData(ranges[0].minIndex);
        }
      }
      this.changedSelection(data);
    }, this);

    this.browse.getBrowse().getTable().getTableModel().addListener('dataChanged', function (e) {
      this.changedSelection(this.browse.getSelectedRowData());
    }, this);
    this.browse.getBrowse().getTable().addListener('nextRecord', function (e) {

      this.changedSelection(this.browse.getSelectedRowData());
    }, this);
    this.browse.getBrowse().getTable().addListener('previousRecord', function (e) {

      this.changedSelection(this.browse.getSelectedRowData());
    }, this);
  }
  changedSelection(rowData) {
    if (rowData.Slika && !rowData.Picture) {
      const picture = this.getBaseUrl() + rowData.Slika;
      picture.replace('File', 'public');
      rowData.Picture = picture;
    }
    if (rowData.Datrojstva) {
      const date = new Date(rowData.Datrojstva);

      rowData.kidsyears = this.calculateAge(date);
    }
    const changes = this.getChanges(rowData, this.currentRow);
    if (Object.entries(changes).length === 0) {
      return;
    }
    this.currentRow = rowData;
    this.currentRowData.next(rowData);
  }
  getStudentData() {
    if (Object.entries(this.currentRow).length === 0) {
      return null;
    }
    return this.currentRow;
  }
  getChanges(data, dataOld) {
    const modifyObj = {};
    for (const key in data) {

      if (typeof data[key] === 'object') {
        continue;
      }
      if (data[key] !== dataOld[key]) {
        modifyObj[key] = data[key];
      }
    }

    return modifyObj;
  }
  calculateAge(birthday) { // birthday is a date
    const ageDifMs = Date.now() - birthday;
    const ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }

  pushMessage(caption, message) {

  }
  loadConstKidSet() {
    if (!this.browse || !this.browse.tableInfo || !this.browse.tableInfo.tables) {
      return;
    }
    const _fields = this.browse.tableInfo.tables[0]._fields;

    if (!_fields) {
      return;
    }

    const consetValues = {};


    if (_fields.EconomicCondition && _fields.EconomicCondition._validValues) {
      consetValues['economicValidValues'] = this.sortArrayOfObjByKey(_fields.EconomicCondition._validValues);
    }

    if (_fields.Support && _fields.Support._validValues) {
      consetValues['supportValidValues'] = this.sortArrayOfObjByKey(_fields.Support._validValues);
    }

    if (_fields.FormOfDailyLiving && _fields.FormOfDailyLiving._validValues) {
      consetValues['formofdailylivingValidValues'] = this.sortArrayOfObjByKey(_fields.FormOfDailyLiving._validValues);
      consetValues['formofdailylivingValidValues'].unshift({ Id: null, Title: 'Ni izbran' });
    }

    this.consetKid = consetValues;
  }
  getConsetSetKid() {
    return this.consetKid;
  }

  private loadConstSets() {
    this.loadTransportConset((data) => { });
    this.loadTeamMembers((data) => { });
    this.loadFactor((data) => { });
    this.loadFunction((data) => { });
    this.loadAdjust((data, data2) => { });
    this.loadAdjustIp((data, data2) => { });
    this.loadGoals((data) => { });
  }


  loadTransportConset(callback) {
    if (this.consetTransport != null) {
      callback(this.consetTransport);
      return;
    }
    const e2Ext = new E2Extension();
    e2Ext.extension = 'CiriusTransportExten';
    e2Ext.args = { type: 'struc' };
    e2Ext.callback = (e) => {
      if (e.result) {
        const data = e.result;
        this.consetTransport = data;
        callback(this.consetTransport);
      }
    };
    e2Ext.cbCtx = this;
    e2Ext.Execute();
  }
  loadTeamMembers(callback) {
    if (this.teamMembers != null) {
      callback(this.teamMembers);
      return;
    }

    const teamExtension = new E2Extension();
    teamExtension.extension = 'CiriusDelavec';
    teamExtension.args = { obtainData: true };
    teamExtension.callback = (e) => {
      if (e && e.result) {
        const jsonData = e.result;
        this.teamMembers = jsonData;


      }
      callback(this.teamMembers);

    };
    teamExtension.cbCtx = this;
    teamExtension.Execute();
  }
  loadFactor(callback) {
    if (this.factorConstSet != null) {
      callback(this.factorConstSet);
      return;
    }
    const e2EntityInfo = new E2Data();
    e2EntityInfo.entity = 'CK_UcenecOkolskiDejavniki';
    e2EntityInfo.format = 'Full';
    e2EntityInfo.callback = function (e) {
      if (e && e.result && e.result.tables) {
        const constsets = {};
        const tables = e.result.tables[0];
        const fields = tables._fields;
        const keys = ['Eko_Biv_Polozaj', 'Pod_Ods_Stal'];
        for (let i = 0; i < keys.length; i++) {
          constsets[keys[i]] = this.helpFromFields(keys[i], fields);
        }
        this.factorConstSet = constsets;
        callback(this.factorConstSet);

      }
    };
    e2EntityInfo.cbCtx = this;
    e2EntityInfo.GetEntityInfo();

  }
  loadFunction(callback) {
    if (this.functionCode != null) {
      callback(this.functionCode);
      return;
    }
    const e2EntityInfo = new E2Data();
    e2EntityInfo.entity = 'CK_FunkcjoniranjeSifrant';
    e2EntityInfo.orderBy = "Type ASC,Order ASC";
    e2EntityInfo.callback = function (e) {
      if (e.result && e.result.data) {
        const data = e.result.data;
        const obj = {};
        for (let i = 0; i < data.length; i++) {
          const key = data[i].Type;
          if (!obj[key]) {
            obj[key] = [];
          }
          obj[key].push(data[i]);

        }
        for (const k in obj) {
          obj[k].unshift({ Id: null, Title: 'Ni izbran' });
        }
        this.functionCode = obj;
        callback(this.functionCode);

      }
    };
    e2EntityInfo.cbCtx = this;
    e2EntityInfo.GetData();


  }
  loadAdjust(callback) {
    if (this.adjustCode != null) {
      callback(this.adjustCode, this.adjustCodeObj);
      return;
    }
    const e2Ext = new E2Extension();
    e2Ext.extension = 'CiriusAdjustments';
    e2Ext.args = { requestType: 'struc' };
    e2Ext.callback = (e) => {
      if (e.result) {
        const data = e.result;
        this.adjustCode = data;

        const obj = {};
        for (const key in data) {
          const inner = {};
          for (let i = 0; i < data[key].length; i++) {
            inner[data[key][i].Id] = data[key][i];
          }
          obj[key] = inner;
        }
        this.adjustCodeObj = obj;
        callback(this.adjustCode, this.adjustCodeObj);
      }
    };
    e2Ext.cbCtx = this;
    e2Ext.Execute();
  }
  loadAdjustIp(callback) {
    if (this.adjustCodeIP != null) {
      callback(this.adjustCodeIP, this.adjustCodeIPObj);
      return;
    }
    const e2Ext = new E2Extension();
    e2Ext.extension = 'CiriusAdjustments';
    e2Ext.args = { requestType: 'struc', ip: true };
    e2Ext.callback = (e) => {
      if (e.result) {
        const data = e.result;
        this.adjustCodeIP = data;

        const obj = {};
        for (const key in data) {
          const inner = {};
          for (let i = 0; i < data[key].length; i++) {
            inner[data[key][i].Id] = data[key][i];
          }
          obj[key] = inner;
        }
        this.adjustCodeIPObj = obj;
        callback(this.adjustCodeIP, this.adjustCodeIPObj);
      }
    };
    e2Ext.cbCtx = this;
    e2Ext.Execute();
  }
  loadGoals(callback) {
    if (this.goalsCode != null) {
      callback(this.goalsCode);
      return;
    }
    const e2EntityInfo = new E2Data();
    e2EntityInfo.entity = 'CK_GoalsCodeList';
    e2EntityInfo.orderBy = 'Type ASC, Order ASC';
    e2EntityInfo.callback = function (e) {
      if (e.result && e.result.data) {
        const data = e.result.data;
        const obj = {};
        for (let i = 0; i < data.length; i++) {
          const key = data[i].Type;
          if (!obj[key]) {
            obj[key] = [];
          }
          obj[key].push(data[i]);

        }
        for (const k in obj) {
          obj[k].unshift({ Id: null, Title: 'Ni izbran' });
        }
        this.goalsCode = obj;
        callback(this.goalsCode);

      }
    };
    e2EntityInfo.cbCtx = this;
    e2EntityInfo.GetData();


  }
  helpFromFields(key, fields) {
    if (fields[key] && fields[key]._validValues) {
      const val = fields[key]._validValues;
      val.unshift({ Id: null, Title: 'Ni izbran' });
      return val;
    }
    return [{ Id: null, Title: 'Ni izbran' }];
  }
  sortArrayOfObjByKey(arr) {
    return arr.sort(function (a, b) {
      if (a['Index'] < b['Index']) {
        return -1;
      }
      if (a['Index'] > b['Index']) {
        return 2;
      }
      return 0;
    });
  }
  sanitazeValues(values, savedObj) {
    const modifyObj = {};
    for (const key in values) {
      if (key == 'Id' || key.includes('E2IF_')) {
        continue;
      }
      if (typeof values[key] === 'object') {
        continue;
      }
      if (savedObj != null) {
        let value = values[key];
        if (value == '') {
          value = null;
        }
        if (value == null && !savedObj[key]) {
          continue;
        }
        if (value != savedObj[key]) {
          modifyObj[key] = values[key];
        }
      }
      else {
        modifyObj[key] = values[key]
      }
    }
    return modifyObj;
  }
}


