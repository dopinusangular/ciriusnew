import { Component, OnInit } from '@angular/core';
import { NotifyChangesService } from '../notify-changes.service';
import { Subscription } from 'rxjs';
import { Student } from '../Student';
declare const E2Extension: any;
@Component({
  selector: 'app-team-panel',
  templateUrl: './team-panel.component.html',
  styleUrls: ['./team-panel.component.css']
})
export class TeamPanelComponent implements OnInit {
  student: Student = new Student(false);
  userSaveDialog = false;
  subscription: Subscription;
  loadedTeam=null;
  teamList = {
     delUcitelj: [{ Id: null,Priimekinime: 'Ni izbran' }],
     delavciVzgojitelj: [{ Id: null,Priimekinime: 'Ni izbran' }],
     delavciZdravnik: [{ Id: null,Priimekinime: 'Ni izbran' }],
     delavciPsiholog: [{ Id: null,Priimekinime: 'Ni izbran' }],
     delavciSocialniDelavec: [{ Id: null,Priimekinime: 'Ni izbran' }],
     delavciVaruhNegovalec: [{ Id: null,Priimekinime: 'Ni izbran' }],
     delavciMedicinskaSestra: [{ Id: null,Priimekinime: 'Ni izbran' }],
     delavciFizioterapevt: [{ Id: null,Priimekinime: 'Ni izbran' }],
     delavciDelavniTerapevt: [{ Id: null,Priimekinime: 'Ni izbran' }],
     delavciLogoped: [{ Id: null,Priimekinime: 'Ni izbran' }]
  };
  mapobject={
    delUcitelj:'UČITELJ',
    delavciVzgojitelj:'VZGOJITELJ',
    delavciZdravnik:'ZDRAVNIK',
    delavciPsiholog:'PSIHOLOG',
    delavciSocialniDelavec:'SOCIALNI DELAVEC',
    delavciVaruhNegovalec:'VARUH/NEGOVALEC',
    delavciMedicinskaSestra:'MEDICINSKA SESTRA',
    delavciFizioterapevt:'FIZIOTERAPEVT',
    delavciDelavniTerapevt:'DELOVNI TERAPEVT',
    delavciLogoped:'LOGOPED'
  }
  kidTeam = {};
  kidTeamOld = {};
  constructor(private notfychangeservice: NotifyChangesService) {
    this.subscription = this.notfychangeservice.getCurrentRow().subscribe(rowData => {
      if (rowData) {
        this.student = new Student(rowData);

      } else {
        // clear messages when empty message received
        this.student = new Student(false);
      }
      this.reloadForm();
    });
    if(this.student.id==null&&this.notfychangeservice.getStudentData()!=null)
    {
      this.student = new Student( this.notfychangeservice.getStudentData());
      this.reloadForm();
    }
  }
  ngOnInit() {
    const that = this;

  }
  reloadForm() {
    if (!this.student.id) {
      this.kidTeam = {};
      this.kidTeamOld = {};
      return;
    }
    if(this.loadedTeam === false) {
      return;
    }
    if(this.loadedTeam === null)
    {
      this.loadedTeam=false;
      this.notfychangeservice.loadTeamMembers((data) => {
        this.getDataByFilter(data);
        this.loadedTeam=true;
        this.reloadForm();
      })

      return;
    }
    const teamExtension = new E2Extension();
    teamExtension.extension = 'CiriusDelavec';
    teamExtension.args = { onStudent: true, student: this.student.id };
    teamExtension.callback = (e) => {
      if (e && e.result) {
        this.setFormData(e.result);
      }
    };
    teamExtension.cbCtx = this;
    teamExtension.Execute();
  }
  setFormData(data) {
    this.kidTeam = data;
    this.kidTeamOld = JSON.parse(JSON.stringify(data));
    this.userSaveDialog = false

  }
  getDataByFilter(data) {
    const that =this;
    Object.keys(this.mapobject).forEach(function(key) {
      that.teamList[key] = data[that.mapobject[key]];
      // ...
  });

  }
  SyncTeam() {
    const teamExtension = new E2Extension();
    teamExtension.extension = 'CiriusDelavec';
    teamExtension.args = { student: this.student.id,workerType: this.kidTeam };
    teamExtension.callback = (e) => {
      if (e && e.result) {
        this.setFormData(this.kidTeam);

      }
    };
    teamExtension.cbCtx = this;
    teamExtension.Execute();
  }
  CancelTeam() {
    this.setFormData(this.kidTeamOld);
  }
  changeTeam() {
    this.userSaveDialog=true;
  }

}
