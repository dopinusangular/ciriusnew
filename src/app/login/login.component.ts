import { Component, OnInit } from '@angular/core';
import { NotifyChangesService } from '../notify-changes.service';
import { Subscription } from 'rxjs';
declare const E2Auth: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  subscription: Subscription;
  loginData={};
  constructor(private notfychangeservice: NotifyChangesService) {
    /*this.subscription = this.notfychangeservice.getCurrentRow().subscribe(rowData => {

    });*/
  }


  ngOnInit() {
  }
  authenticate(){
    this.notfychangeservice.authenticate(this.loginData['Username'],this.loginData['Password'])
  }
}
