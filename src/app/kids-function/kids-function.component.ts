import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotifyChangesService } from '../notify-changes.service';
import { Subscription } from 'rxjs';
import { Student } from '../Student';
declare const E2Data: any;
declare const E2Extension: any;
@Component({
  selector: 'app-kids-function',
  templateUrl: './kids-function.component.html',
  styleUrls: ['./kids-function.component.css']
})
export class KidsFunctionComponent implements OnDestroy {
  student: Student = new Student(false);
  userSaveDialog = false;
  subscription: Subscription;
  loadedfunction = null;
  FunctionCode = {};
  kidFunction = {};
  kidFunctionOld = {};
  // tslint:disable-next-line: max-line-length
  saveButton = { SolIzob_Ucenje: 'Spremeni', Gib_Mob: 'Spremeni', ONiZ: 'Spremeni', Sporazumevanje: 'Spremeni', Med_odnosi: 'Spremeni', SocVesc: 'Spremeni' };
  editMode = { SolIzob_Ucenje: false, Gib_Mob: false, ONiZ: false, Sporazumevanje: false, Med_odnosi: false, SocVesc: false };
  constructor(private notfychangeservice: NotifyChangesService) {
    this.subscription = this.notfychangeservice.getCurrentRow().subscribe(rowData => {
      if (rowData) {
        this.student = new Student(rowData);
      } else {
        // clear messages when empty message received
        this.student = new Student(false);
      }
      this.reloadForm();
    });
    if (this.student.id == null && this.notfychangeservice.getStudentData() != null) {
      this.student = new Student(this.notfychangeservice.getStudentData());
      this.reloadForm();
    }
  }

  ngOnDestroy() {
    console.log("destroy function");
    this.subscription.unsubscribe();
    this.kidFunction = null;
    this.kidFunction = null;
    this.student=null;

  }
  reloadForm() {
    console.log(this.student);
    if (!this.student.id) {
      this.kidFunction = {};
      this.kidFunctionOld = {};
      return;
    }
    if (this.loadedfunction === false) {
      return;
    }
    if (this.loadedfunction === null) {
      this.loadedfunction = false;
      this.notfychangeservice.loadFunction((data) => {
        this.FunctionCode = data;
        console.log(data);
        this.loadedfunction = true;
        this.reloadForm();
      });
      return;
    }
    const e2Ext = new E2Extension();
    e2Ext.extension = 'CiriusFunctionExten';
    e2Ext.args = { type: 'get', student: this.student.id };
    e2Ext.callback = (e) => {
      if (e.result) {
        this.setFormData(e.result);
      }  else {
        this.setFormData({});
      }

    };
    e2Ext.cbCtx = this;
    e2Ext.Execute();
  }
  setFormData(data) {
    console.log(data);
    this.kidFunction = data;
    this.kidFunctionOld = JSON.parse(JSON.stringify(data));
    this.userSaveDialog = false;

  }
  savePorocilo(value) {
    if (this.editMode[value] == true) {
      this.editMode[value] = false;
      this.saveButton[value] = 'Spremeni';
      this.SaveOrAddUserOnClick(true, value + '_Porocilo');
    } else {
      this.editMode[value] = true;
      this.saveButton[value] = 'Shrani';
    }
  }
  cancelPorocilo(value) {
    this.editMode[value] = false;
    this.saveButton[value] = 'Spremeni';
    this.kidFunction[value] = this.kidFunctionOld[value];
  }
  CancelUserOnClick() {
    this.setFormData(this.kidFunctionOld);
  }
  SaveOrAddUserOnClick(partial, key) {
    if (this.student.id) {
      let obj = {};
      if (partial) {
        obj[key] = this.kidFunction[key];
      }  else {
        obj = this.sanitazeValues(this.kidFunction, this.kidFunctionOld);
      }
      const e2Ext = new E2Extension();
      e2Ext.extension = 'CiriusFunctionExten';
      e2Ext.args = { type: 'sync', student: this.student.id, syncObj: obj };
      e2Ext.callback = (e) => {
        if (e.result) {
          this.notfychangeservice.pushMessage('Opozorilo', 'Shranjevanje je uspešno.');
          if (key) {
            this._SaveCurrentState(this.kidFunction, key);
          } else {
            this.setFormData(this.kidFunction);
            this.userSaveDialog = false;
          }
        } else {
          this.notfychangeservice.pushMessage('Opozorilo', 'Shranjevanje ni bilo uspešno. Error:' + e.error);
        }
      };
      e2Ext.cbCtx = this;


      e2Ext.Execute();


    }

  }
  _SaveCurrentState(data, key) {
    if (data != null) {
      if (!key) {
        this.kidFunctionOld = JSON.parse(JSON.stringify(data));
      }  else {
        this.kidFunctionOld[key] = data[key];
      }
    }  else {
      this.kidFunctionOld = {};
    }

  }
  sanitazeValues(values, savedObj) {
    const modifyObj = {};
    for (const key in values) {
      if (key == 'Id' || key.includes('E2IF_')) {
        continue;
      }
      if (typeof values[key] === 'object') {
        continue;
      }
      if (savedObj != null) {
        let value = values[key];
        if (value == '') {
          value = null;
        }
        if (value == null && !savedObj[key]) {
          continue;
        }
        if (value != savedObj[key]) {
          modifyObj[key] = values[key];
        }
      }
      else {
        modifyObj[key] = values[key]
      }
    }
    return modifyObj;
  }
  changeModel() {
    this.userSaveDialog = true;
  }
}
