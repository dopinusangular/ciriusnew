import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KidsFunctionComponent } from './kids-function.component';

describe('KidsFunctionComponent', () => {
  let component: KidsFunctionComponent;
  let fixture: ComponentFixture<KidsFunctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KidsFunctionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KidsFunctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
